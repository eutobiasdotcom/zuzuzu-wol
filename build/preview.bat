@echo off

::SETTINGS
	set ADL_PATH=C:\Work\tools\flex.3.5.0\bin
	set PROJECT_NAME=WOL
	
:: ON WIN XP
	:: tskill /im adl

:: ON WIN VISTA/SEVEN 
	taskkill /im adl.exe

::Compile
	start /MIN  %ADL_PATH%\adl.exe build\application.xml bin\
