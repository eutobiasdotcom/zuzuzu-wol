@echo off
	
:: ##########################################
:: SETTINGS
:: ##########################################

::AIR PATH SEM \ ou espaços vazios no final
	set AIR_PATH=C:/Work/tools/flex.3.5.0/bin
	
::PROJECT GLOBAL SETTINGS
	
	set PROJECT_NAME=WOL
	set PROJECT_VERSION=1.0.0
	
::APPLICATION SPECIFIC SETTINGS
	
	set OUTPUT_SWF_FILE=../bin/WOL.swf
	set OUTPUT_AIR_FILE=../air/ZuzuzuWOL-%PROJECT_VERSION%.air
	
	set ALWAYS_COMPILE_FILE=../src/Main.mxml
	set APPLICATION_XML_FILE=application.xml
	set APPLICATION_STORE_PATH=../bin/
	
	
:: ##########################################
:: DON´T EDIT BELOW THIS LINE
:: ##########################################
	
::SETUP
	if not exist ../air md ../air
	if exist TB.pfx goto COMPILE
	if not exist TB.pfx goto CERTIFICATE

:CERTIFICATE
	echo ... certificating...
	call %AIR_PATH%\adt -certificate -cn TB 1024-RSA TB.pfx #@B -storetype pkcs12 -keystore TB.pfx -storepass #@B
	if errorlevel 1 goto FAIL
	goto COMPILE

:COMPILE
	echo ... compiling...
	call %AIR_PATH%\mxmlc +configname=air -optimize=true -debug=false -load-config+=../obj/%PROJECT_NAME%Config.xml %ALWAYS_COMPILE_FILE% -o %OUTPUT_SWF_FILE%
	if errorlevel 1 goto FAIL
	goto PACK
	
:PACK
	echo ... packing...
	call %AIR_PATH%\adt -package -storetype pkcs12 -tsa none -keystore TB.pfx -storepass #@B %OUTPUT_AIR_FILE% %APPLICATION_XML_FILE% -C %APPLICATION_STORE_PATH% .
	if errorlevel 1 goto FAIL
	goto END
	
:FAIL
	echo ... fail ...
	goto END
	
:END
	pause